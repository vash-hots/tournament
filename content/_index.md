![Let's rock!](./title.png)
# Déroulement

Le tournoi se joue en deux phases :

* Une phase de groupe à tirage aléatoire durant laquelle tous les joueurs d'un groupe se rencontrent à une occasion pour deux matches.
* Une phase de [playoffs à double élimination](https://en.wikipedia.org/wiki/Double-elimination_tournament) en [BO3](https://idioms.thefreedictionary.com/Best+of+Three) avec [simple grande finale](https://help.toornament.com/structures/double-finals).

Les résultats des matches sont consultables sur [Toornament](https://www.toornament.com/fr/tournaments/4570307895900504064).

Profitez du [serveur Discord dédié](https://discord.gg/5ntbagSx) pour faciliter la communication.
    
# Inscription

Sur [Toornament](https://www.toornament.com/fr/tournaments/4570307895900504064/registration/), en utilisant votre pseudo dans le jeu.
# Règlement

Les héros suivants sont exclus : Abathur, Azmodan, Cho, Gall, Ragnaros, The Lost Vikings, Zagara.

## Règles Générales

* La carte est `Cursed Hollow (Sandbox)`.
* Le héros est choisi dans la liste des 7 héros imposée pour le match.
* Chaque joueur choisit son héros sans connaître le choix de son adversaire. Tous les mécanismes sont autorisés pour arriver à cette fin : arbitre indépendant, annonce simultanée, pigeon voyageur, ... 
* Interdiction sous peine de forfait :
   * Retourner à la zone de spawn, sauf en cas de décès (pas de hearthstone, pas de rocket de Junkrat, ...).
   * Quitter le couloir du milieu. Les boucles au nord et au sud du centre du couloir sont autorisées.
   * Retourner à sa fontaine sans passer par le mur, i.e., ne pas s'échapper par la jungle alliée.
   * Interagir avec les mercenaires.
   * Passer derrière le fort ennemi, sauf pour s'échapper bien entendu.
* Victoire en trois meurtres ou destruction des deux tours de défense.

## Phase de Groupe

* 1 point par match gagné lors d'une rencontre. Les possibilités sont donc `2-0`, `1-1` et `0-2`.
* En cas d'égalité au classement général, on tranchera à la différence meurtres-décès, puis par pile ou face si nécessaire.

## Phase de Playoffs

* La moitié supérieure du tableau de classement des groupes commence dans le winner bracket, la moitié inférieure dans le loser bracket (aka [skip first round](https://help.toornament.com/structures/introducing-the-skip-1st-round)).
* Au sein de chaque bracket, le placement sera effectué afin que les joueurs les mieux classés se rencontrent le plus tard possible (cf. [seeding](https://en.wikipedia.org/wiki/Seed_(sports))).
* Victoire en BO3 jusqu'aux finales, BO5 pour les trois finales.
# Enjeu
 
![Beautiful](./cupAndRainbow.png)
# Les Groupes

{{<table "table">}}
| Groupe 1 | Groupe 2 |
| --- | --- |
| BEURNZZZZ | Garandun |
| Graigzz | Kör |
| Twisterwind | Moun |
| Vash | MrDrouf |
| Zoiedberg | Naleenae |
{{</table>}}

## Matches

### Groupe 1
* __BEURNZZZZ__ vs __Graigzz__
   1. _Muradin_ - _Xul_ - _Artanis_ - _Nova_ - _Anduin_ - _D.Va_ - _Alarak_
   2. _Kael'thas_ - _Sgt. Hammer_ - _Tracer_ - _Alexstrasza_ - _Lt. Morales_ - _Tyrande_ - _Orphea_
* __BEURNZZZZ__ vs __Twisterwind__
   1. _Imperius_ - _Jaina_ - _Tassadar_ - _Sylvanas_ - _Malfurion_ - _Qhira_ - _Zeratul_
   2. _Anub'arak_ - _Tyrael_ - _Medivh_ - _Malfurion_ - _Varian_ - _Maiev_ - _Zeratul_
* __BEURNZZZZ__ vs __Vash__
   1. _Uther_ - _Gul'dan_ - _Jaina_ - _Anub'arak_ - _Dehaka_ - _Lúcio_ - _Chromie_
   2. _Malthael_ - _Valeera_ - _Mephisto_ - _Sylvanas_ - _Tracer_ - _Junkrat_ - _Chromie_
* __BEURNZZZZ__ vs __Zoiedberg__
   1. _Imperius_ - _Lúcio_ - _Hogger_ - _Samuro_ - _Deathwing_ - _Nova_ - _Malfurion_
   2. _Kharazim_ - _Jaina_ - _Tyrael_ - _Kael'thas_ - _Malfurion_ - _Thrall_ - _Tyrande_
* __Graigzz__ vs __Twisterwind__
   1. _Artanis_ - _Sgt. Hammer_ - _Li-Ming_ - _Tyrande_ - _Muradin_ - _Kael'thas_ - _Tassadar_
   2. _Illidan_ - _Whitemane_ - _Deathwing_ - _Hanzo_ - _Mei_ - _Sonya_ - _Junkrat_
* __Graigzz__ vs __Vash__
   1. _Whitemane_ - _Tyrael_ - _Falstad_ - _Qhira_ - _Alexstrasza_ - _Garrosh_ - _Tychus_
   2. _Zarya_ - _Cassia_ - _Brightwing_ - _Kel'Thuzad_ - _Sylvanas_ - _Tracer_ - _Samuro_
* __Graigzz__ vs __Zoiedberg__
   1. _Probius_ - _Malthael_ - _Anub'arak_ - _Whitemane_ - _Kael'thas_ - _Yrel_ - _Tassadar_
   2. _Samuro_ - _Zeratul_ - _Lt. Morales_ - _Ana_ - _Uther_ - _Johanna_ - _Hogger_
* __Twisterwind__ vs __Vash__
   1. _Mal'Ganis_ - _Probius_ - _Genji_ - _Gazlowe_ - _Samuro_ - _Gul'dan_ - _Dehaka_
   2. _Chromie_ - _Diablo_ - _Tyrande_ - _Mal'Ganis_ - _The Butcher_ - _Muradin_ - _D.Va_
* __Twisterwind__ vs __Zoiedberg__
   1. _Kel'Thuzad_ - _Qhira_ - _Alarak_ - _Deckard_ - _Nova_ - _Blaze_ - _Medivh_
   2. _Ana_ - _Kael'thas_ - _Artanis_ - _Diablo_ - _Genji_ - _Sylvanas_ - _Sonya_
* __Vash__ vs __Zoiedberg__
   1. _Gul'dan_ - _Stitches_ - _The Butcher_ - _Rehgar_ - _Tyrande_ - _Xul_ - _Alexstrasza_
   2. _Brightwing_ - _Gazlowe_ - _Falstad_ - _Tracer_ - _Maiev_ - _Artanis_ - _Jaina_
### Groupe 2
* __Garandun__ vs __Kör__
   1. _Auriel_ - _Mei_ - _Deckard_ - _Ana_ - _Chen_ - _Li Li_ - _Arthas_
   2. _Tychus_ - _Li-Ming_ - _Jaina_ - _Xul_ - _Thrall_ - _Falstad_ - _Zul'jin_
* __Garandun__ vs __Moun__
   1. _Valeera_ - _Kel'Thuzad_ - _Yrel_ - _Uther_ - _Cassia_ - _Tyrande_ - _Sonya_
   2. _Maiev_ - _Tyrael_ - _Arthas_ - _Deathwing_ - _Nazeebo_ - _Nova_ - _Malfurion_
* __Garandun__ vs __MrDrouf__
   1. _Li Li_ - _Tyrande_ - _Sonya_ - _Varian_ - _Diablo_ - _Blaze_ - _Sylvanas_
   2. _Deathwing_ - _Rexxar_ - _Rehgar_ - _Ana_ - _Probius_ - _Tyrande_ - _Tassadar_
* __Garandun__ vs __Naleenae__
   1. _Blaze_ - _Kharazim_ - _Sonya_ - _Stukov_ - _Deathwing_ - _Johanna_ - _Malfurion_
   2. _Lúcio_ - _Garrosh_ - _Rexxar_ - _Ana_ - _Valla_ - _Chromie_ - _Rehgar_
* __Kör__ vs __Moun__
   1. _Rehgar_ - _Kerrigan_ - _Tracer_ - _Anduin_ - _Tyrael_ - _Greymane_ - _Kharazim_
   2. _Artanis_ - _Kael'thas_ - _Tracer_ - _Sgt. Hammer_ - _Mephisto_ - _Blaze_ - _D.Va_
* __Kör__ vs __MrDrouf__
   1. _Tassadar_ - _Valla_ - _Varian_ - _Sgt. Hammer_ - _Thrall_ - _Malthael_ - _Valeera_
   2. _Li-Ming_ - _Nazeebo_ - _Hanzo_ - _Chen_ - _D.Va_ - _Sylvanas_ - _Zul'jin_
* __Kör__ vs __Naleenae__
   1. _Murky_ - _Lúcio_ - _Thrall_ - _Malthael_ - _D.Va_ - _Chen_ - _Orphea_
   2. _Malthael_ - _Arthas_ - _Mal'Ganis_ - _Valeera_ - _Auriel_ - _Muradin_ - _Kel'Thuzad_
* __Moun__ vs __MrDrouf__
   1. _Junkrat_ - _Hanzo_ - _Leoric_ - _Zeratul_ - _Rexxar_ - _Li Li_ - _Qhira_
   2. _Qhira_ - _Lt. Morales_ - _Auriel_ - _Falstad_ - _Valeera_ - _Sgt. Hammer_ - _Alarak_
* __Moun__ vs __Naleenae__
   1. _Anduin_ - _Orphea_ - _Rehgar_ - _Tassadar_ - _Qhira_ - _Junkrat_ - _Leoric_
   2. _D.Va_ - _Li Li_ - _Deckard_ - _Garrosh_ - _Orphea_ - _Maiev_ - _Stitches_
* __MrDrouf__ vs __Naleenae__
   1. _Orphea_ - _Jaina_ - _Stukov_ - _Tyrande_ - _Sylvanas_ - _Kael'thas_ - _Lt. Morales_
   2. _Tyrael_ - _Ana_ - _Whitemane_ - _Mei_ - _Varian_ - _Valla_ - _Xul_
# Playoffs

    
##### WB1.1

   1. _Valeera_ - _Illidan_ - _Samuro_ - _Cassia_ - _Diablo_ - _Muradin_ - _Zul'jin_
   2. _Imperius_ - _Deathwing_ - _Xul_ - _Sylvanas_ - _Arthas_ - _Fenix_ - _Kerrigan_
   3. _Illidan_ - _Sonya_ - _Alarak_ - _Genji_ - _Rexxar_ - _Blaze_ - _Malthael_
##### WB1.2

   1. _Sgt. Hammer_ - _Alarak_ - _Zul'jin_ - _Yrel_ - _Rexxar_ - _Kerrigan_ - _Murky_
   2. _Rexxar_ - _Garrosh_ - _Stitches_ - _Falstad_ - _Cassia_ - _Sonya_ - _Junkrat_
   3. _Sylvanas_ - _Falstad_ - _Illidan_ - _Yrel_ - _Leoric_ - _Kel'Thuzad_ - _Mephisto_
##### WB1.3

   1. _Diablo_ - _Sonya_ - _Zarya_ - _Nazeebo_ - _Kael'thas_ - _Rexxar_ - _Tyrael_
   2. _Johanna_ - _Murky_ - _Sonya_ - _Deathwing_ - _D.Va_ - _Valla_ - _Qhira_
   3. _Xul_ - _Nova_ - _Falstad_ - _Lunara_ - _Li-Ming_ - _Sonya_ - _Cassia_
##### WB1.4

   1. _Qhira_ - _Arthas_ - _Sonya_ - _Dehaka_ - _Mephisto_ - _Fenix_ - _Maiev_
   2. _Junkrat_ - _Zarya_ - _Lunara_ - _Muradin_ - _Zeratul_ - _Mephisto_ - _Chromie_
   3. _Chromie_ - _Medivh_ - _Illidan_ - _Xul_ - _Probius_ - _Garrosh_ - _Johanna_
##### LB1.1

   1. _Valla_ - _Alarak_ - _Stitches_ - _Leoric_ - _Murky_ - _Varian_ - _Malthael_
   2. _Rexxar_ - _Falstad_ - _Orphea_ - _D.Va_ - _Dehaka_ - _Mei_ - _Chen_
   3. _Leoric_ - _Imperius_ - _Dehaka_ - _Greymane_ - _Lunara_ - _Zarya_ - _Raynor_
##### LB1.2

   1. _Hogger_ - _Deathwing_ - _Diablo_ - _Gazlowe_ - _Mei_ - _Johanna_ - _Thrall_
   2. _Probius_ - _Garrosh_ - _Tassadar_ - _Mei_ - _Malthael_ - _Murky_ - _Fenix_
   3. _Probius_ - _Diablo_ - _Murky_ - _Falstad_ - _Artanis_ - _E.T.C._ - _Stitches_
##### WB2.1

   1. _Genji_ - _Nova_ - _Diablo_ - _Murky_ - _Sgt. Hammer_ - _Qhira_ - _Zeratul_
   2. _The Butcher_ - _Zarya_ - _Qhira_ - _Zul'jin_ - _Murky_ - _Kel'Thuzad_ - _Kael'thas_
   3. _Sylvanas_ - _Xul_ - _Artanis_ - _Jaina_ - _Varian_ - _Hanzo_ - _Arthas_
##### WB2.2

   1. _The Butcher_ - _E.T.C._ - _Tracer_ - _Garrosh_ - _Chen_ - _Zul'jin_ - _Hogger_
   2. _Thrall_ - _Chromie_ - _Nova_ - _Imperius_ - _Tyrael_ - _Sylvanas_ - _Zarya_
   3. _Sylvanas_ - _Valla_ - _Zeratul_ - _Garrosh_ - _Valeera_ - _Xul_ - _Fenix_
##### LB2.1

   1. _Johanna_ - _Arthas_ - _Yrel_ - _Nazeebo_ - _Tyrael_ - _Deathwing_ - _Orphea_
   2. _Hanzo_ - _Mei_ - _Deathwing_ - _Chen_ - _Rexxar_ - _Tassadar_ - _Tychus_
   3. _Mephisto_ - _Cassia_ - _Alarak_ - _Lunara_ - _Illidan_ - _Tychus_ - _E.T.C._
##### LB2.2

   1. _Kael'thas_ - _Rexxar_ - _Zeratul_ - _Genji_ - _E.T.C._ - _Varian_ - _Kel'Thuzad_
   2. _Blaze_ - _Mephisto_ - _Zeratul_ - _Tassadar_ - _Chromie_ - _Kael'thas_ - _Yrel_
   3. _Tassadar_ - _Probius_ - _Yrel_ - _Lunara_ - _Artanis_ - _Raynor_ - _Medivh_
##### LB3.1

   1. _Xul_ - _Kerrigan_ - _Mal'Ganis_ - _Stitches_ - _Arthas_ - _Thrall_ - _Anub'arak_
   2. _Sonya_ - _Jaina_ - _Tassadar_ - _Raynor_ - _Maiev_ - _Cassia_ - _Illidan_
   3. _Orphea_ - _Tracer_ - _D.Va_ - _Zarya_ - _Xul_ - _Leoric_ - _Genji_
##### LB3.2

   1. _D.Va_ - _Hanzo_ - _Junkrat_ - _Kerrigan_ - _Valeera_ - _Kael'thas_ - _Leoric_
   2. _Johanna_ - _Mei_ - _Alarak_ - _Fenix_ - _Mephisto_ - _Medivh_ - _Sylvanas_
   3. _Anub'arak_ - _Lunara_ - _Kael'thas_ - _Rexxar_ - _Leoric_ - _Tyrael_ - _Falstad_
##### LB4.1

   1. _Nova_ - _Stitches_ - _Blaze_ - _Tassadar_ - _Hanzo_ - _Zeratul_ - _Varian_
   2. _Probius_ - _Alarak_ - _Falstad_ - _Artanis_ - _Gul'dan_ - _Sonya_ - _Arthas_
   3. _Kael'thas_ - _Alarak_ - _Blaze_ - _Chen_ - _Jaina_ - _Tychus_ - _Samuro_
##### WB Final

   1. _Valeera_ - _Jaina_ - _Diablo_ - _Malthael_ - _Valla_ - _Orphea_ - _Zul'jin_
   2. _Sonya_ - _Zeratul_ - _Probius_ - _Blaze_ - _Tracer_ - _Yrel_ - _Gul'dan_
   3. _Muradin_ - _Chen_ - _Tracer_ - _Valeera_ - _Junkrat_ - _Raynor_ - _Varian_
   4. _Nova_ - _Diablo_ - _Lunara_ - _Tassadar_ - _Imperius_ - _Valla_ - _Chen_
   5. _Samuro_ - _Zul'jin_ - _Illidan_ - _Mei_ - _Anub'arak_ - _Xul_ - _Medivh_
##### LB Final

   1. _Jaina_ - _Gazlowe_ - _Kel'Thuzad_ - _Diablo_ - _Alarak_ - _Kael'thas_ - _Varian_
   2. _Tassadar_ - _The Butcher_ - _Gazlowe_ - _Gul'dan_ - _Sgt. Hammer_ - _Raynor_ - _Rexxar_
   3. _Samuro_ - _Tyrael_ - _Maiev_ - _Greymane_ - _Blaze_ - _Artanis_ - _Mei_
   4. _Leoric_ - _Gazlowe_ - _Deathwing_ - _Artanis_ - _D.Va_ - _Chromie_ - _Maiev_
   5. _Gazlowe_ - _Mei_ - _Tassadar_ - _Xul_ - _Muradin_ - _The Butcher_ - _Zeratul_
##### Grand Final

   1. _Arthas_ - _Hogger_ - _Greymane_ - _Kerrigan_ - _The Butcher_ - _Chromie_ - _Deathwing_
   2. _Medivh_ - _Varian_ - _Xul_ - _Tyrael_ - _Sylvanas_ - _The Butcher_ - _Mephisto_
   3. _Muradin_ - _Sylvanas_ - _Tychus_ - _Imperius_ - _Alarak_ - _Kael'thas_ - _Stitches_
   4. _Xul_ - _Zul'jin_ - _Sylvanas_ - _Orphea_ - _Imperius_ - _Malthael_ - _Gul'dan_
   5. _Muradin_ - _Sonya_ - _Samuro_ - _Blaze_ - _Zul'jin_ - _Zarya_ - _Maiev_