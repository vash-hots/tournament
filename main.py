import random
import fire
from pyfiglet import Figlet
from typing import List, Any
from PIL import Image, ImageDraw, ImageFont
import itertools


random.seed(47)

HEROES_NUMBER = 7
GROUPS_NUMBER = 2
PLAYERS = [
    "BEURNZZZZ",
    "Garandun",
    "Graigzz",
    "Kör",
    "Moun",
    "MrDrouf",
    "Naleenae",
    "Twisterwind",
    "Vash",
    "Zoiedberg",
]
random.shuffle(PLAYERS)

HEROES = set(
    [
        "Abathur",
        "Alarak",
        "Alexstrasza",
        "Ana",
        "Anduin",
        "Anub'arak",
        "Artanis",
        "Arthas",
        "Auriel",
        "Azmodan",
        "Blaze",
        "Brightwing",
        "Cassia",
        "Chen",
        "Cho",
        "Chromie",
        "D.Va",
        "Deathwing",
        "Deckard",
        "Dehaka",
        "Diablo",
        "E.T.C.",
        "Falstad",
        "Fenix",
        "Gall",
        "Garrosh",
        "Gazlowe",
        "Genji",
        "Greymane",
        "Gul'dan",
        "Hanzo",
        "Hogger",
        "Illidan",
        "Imperius",
        "Jaina",
        "Johanna",
        "Junkrat",
        "Kael'thas",
        "Kel'Thuzad",
        "Kerrigan",
        "Kharazim",
        "Leoric",
        "Li Li",
        "Li-Ming",
        "Lt. Morales",
        "Lunara",
        "Lúcio",
        "Maiev",
        "Mal'Ganis",
        "Malfurion",
        "Malthael",
        "Medivh",
        "Mei",
        "Mephisto",
        "Muradin",
        "Murky",
        "Nazeebo",
        "Nova",
        "Orphea",
        "Probius",
        "Qhira",
        "Ragnaros",
        "Raynor",
        "Rehgar",
        "Rexxar",
        "Samuro",
        "Sgt. Hammer",
        "Sonya",
        "Stitches",
        "Stukov",
        "Sylvanas",
        "Tassadar",
        "The Butcher",
        "The Lost Vikings",
        "Thrall",
        "Tracer",
        "Tychus",
        "Tyrael",
        "Tyrande",
        "Uther",
        "Valeera",
        "Valla",
        "Varian",
        "Whitemane",
        "Xul",
        "Yrel",
        "Zagara",
        "Zarya",
        "Zeratul",
        "Zul'jin",
    ]
)
SECOND_BAN_WAVE = set(
    [
        "Alexstrasza",
        "Ana",
        "Anduin",
        "Auriel",
        "Brightwing",
        "Deckard",
        "Kharazim",
        "Li Li",
        "Lt. Morales",
        "Lúcio",
        "Malfurion",
        "Rehgar",
        "Stukov",
        "Tyrande",
        "Uther",
        "Whitemane",
    ]
)
EXCLUDED = sorted(
    ["Azmodan", "Zagara", "The Lost Vikings", "Abathur", "Ragnaros", "Cho", "Gall"]
)
HEROES = sorted(list(HEROES - set(EXCLUDED)))


def split_list(elements: List[Any], n: int):
    k, m = divmod(len(elements), n)
    return list(
        elements[i * k + min(i, m) : (i + 1) * k + min(i + 1, m)] for i in range(n)
    )


def write_page():
    # Save title image
    img = Image.new("RGB", (600, 500), color=(255, 255, 255))
    fnt = ImageFont.truetype("./Ubuntu-Mono-derivative-Powerline-Bold.ttf", 20)
    d = ImageDraw.Draw(img)
    figlet = Figlet(font="big", width=70)
    title = "\n".join(
        x.center(70)
        for x in figlet.renderText("1vs1   HOTS   King Tournament").split("\n")
    )
    d.text((-50, 10), title, font=fnt, fill=(0, 0, 0))
    img.save("static/title.png")
    title_text = "![Let's rock!](./title.png)"

    # Inscription
    inscription_text = f"""
# Inscription

Sur [Toornament](https://www.toornament.com/fr/tournaments/4570307895900504064/registration/), en utilisant votre pseudo dans le jeu."""
    # Déroulement
    flow_text = f"""
# Déroulement

Le tournoi se joue en deux phases :

* Une phase de groupe à tirage aléatoire durant laquelle tous les joueurs d'un groupe se rencontrent à une occasion pour deux matches.
* Une phase de [playoffs à double élimination](https://en.wikipedia.org/wiki/Double-elimination_tournament) en [BO3](https://idioms.thefreedictionary.com/Best+of+Three) avec [simple grande finale](https://help.toornament.com/structures/double-finals).

Les résultats des matches sont consultables sur [Toornament](https://www.toornament.com/fr/tournaments/4570307895900504064).

Profitez du [serveur Discord dédié](https://discord.gg/5ntbagSx) pour faciliter la communication.
    """

    # Rules
    rules_text = f"""
# Règlement

Les héros suivants sont exclus : {", ".join(EXCLUDED)}.

## Règles Générales

* La carte est `Cursed Hollow (Sandbox)`.
* Le héros est choisi dans la liste des {HEROES_NUMBER} héros imposée pour le match.
* Chaque joueur choisit son héros sans connaître le choix de son adversaire. Tous les mécanismes sont autorisés pour arriver à cette fin : arbitre indépendant, annonce simultanée, pigeon voyageur, ... 
* Interdiction sous peine de forfait :
   * Retourner à la zone de spawn, sauf en cas de décès (pas de hearthstone, pas de rocket de Junkrat, ...).
   * Quitter le couloir du milieu. Les boucles au nord et au sud du centre du couloir sont autorisées.
   * Retourner à sa fontaine sans passer par le mur, i.e., ne pas s'échapper par la jungle alliée.
   * Interagir avec les mercenaires.
   * Passer derrière le fort ennemi, sauf pour s'échapper bien entendu.
* Victoire en trois meurtres ou destruction des deux tours de défense.

## Phase de Groupe

* 1 point par match gagné lors d'une rencontre. Les possibilités sont donc `2-0`, `1-1` et `0-2`.
* En cas d'égalité au classement général, on tranchera à la différence meurtres-décès, puis par pile ou face si nécessaire.

## Phase de Playoffs

* La moitié supérieure du tableau de classement des groupes commence dans le winner bracket, la moitié inférieure dans le loser bracket (aka [skip first round](https://help.toornament.com/structures/introducing-the-skip-1st-round)).
* Au sein de chaque bracket, le placement sera effectué afin que les joueurs les mieux classés se rencontrent le plus tard possible (cf. [seeding](https://en.wikipedia.org/wiki/Seed_(sports))).
* Victoire en BO3 jusqu'aux finales, BO5 pour les trois finales."""
    # Enjeu
    cup_text = """
# Enjeu
 
![Beautiful](./cupAndRainbow.png)"""

    # Group phase
    groups = [sorted(x) for x in split_list(elements=PLAYERS, n=GROUPS_NUMBER)]
    groups_text = f"""
# Les Groupes

{{{{<table "table">}}}}
| {" | ".join(f"Groupe {n+1}" for n in range(len(groups)))} |
| {" | ".join(f"---" for _ in range(len(groups)))} |"""

    for i in range(len(groups[0])):
        groups_text = (
            groups_text
            + f"""
| {" | ".join(group[i] for group in groups)} |"""
        )
    groups_text += "\n{{</table>}}"

    groups_matches_text = f"""

## Matches
"""
    for i, group in enumerate(groups):
        groups_matches_text += f"\n### Groupe {i+1}"
        for player1, player2 in itertools.combinations(group, 2):
            groups_matches_text += f"\n* __{player1}__ vs __{player2}__"
            groups_matches_text += (
                f"\n   1. _{'_ - _'.join(random.sample(HEROES, k=HEROES_NUMBER))}_"
            )
            groups_matches_text += (
                f"\n   2. _{'_ - _'.join(random.sample(HEROES, k=HEROES_NUMBER))}_"
            )
    # Playoffs

    playoffs_text = f"""
# Playoffs

    """
    PLAYOFFS_HEROES = sorted(list(set(HEROES) - SECOND_BAN_WAVE))
    PLAYOFF_MATCHES = [
        "WB1.1",
        "WB1.2",
        "WB1.3",
        "WB1.4",
        "LB1.1",
        "LB1.2",
        "WB2.1",
        "WB2.2",
        "LB2.1",
        "LB2.2",
        "LB3.1",
        "LB3.2",
        "LB4.1",
    ]
    PLAYOFF_FINALS = ["WB Final", "LB Final", "Grand Final"]
    for playoff_match in PLAYOFF_MATCHES:
        playoffs_text += f"\n##### {playoff_match}\n"
        playoffs_text += (
            f"\n   1. _{'_ - _'.join(random.sample(PLAYOFFS_HEROES, k=HEROES_NUMBER))}_"
        )
        playoffs_text += (
            f"\n   2. _{'_ - _'.join(random.sample(PLAYOFFS_HEROES, k=HEROES_NUMBER))}_"
        )
        playoffs_text += (
            f"\n   3. _{'_ - _'.join(random.sample(PLAYOFFS_HEROES, k=HEROES_NUMBER))}_"
        )

    for playoff_match in PLAYOFF_FINALS:
        playoffs_text += f"\n##### {playoff_match}\n"
        playoffs_text += (
            f"\n   1. _{'_ - _'.join(random.sample(PLAYOFFS_HEROES, k=HEROES_NUMBER))}_"
        )
        playoffs_text += (
            f"\n   2. _{'_ - _'.join(random.sample(PLAYOFFS_HEROES, k=HEROES_NUMBER))}_"
        )
        playoffs_text += (
            f"\n   3. _{'_ - _'.join(random.sample(PLAYOFFS_HEROES, k=HEROES_NUMBER))}_"
        )
        playoffs_text += (
            f"\n   4. _{'_ - _'.join(random.sample(PLAYOFFS_HEROES, k=HEROES_NUMBER))}_"
        )
        playoffs_text += (
            f"\n   5. _{'_ - _'.join(random.sample(PLAYOFFS_HEROES, k=HEROES_NUMBER))}_"
        )

    with open("./content/_index.md", "w") as page:
        page.write(title_text)
        page.write(flow_text)
        page.write(inscription_text)
        page.write(rules_text)
        page.write(cup_text)
        page.write(groups_text)
        page.write(groups_matches_text)
        page.write(playoffs_text)


if __name__ == "__main__":
    fire.Fire(write_page)
