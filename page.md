```
 __          __     _    _  ____ _______ _____     _  ___             
/_ |        /_ |   | |  | |/ __ \__   __/ ____|   | |/ (_)            
 | |_   _____| |   | |__| | |  | | | | | (___     | ' / _ _ __   __ _ 
 | \ \ / / __| |   |  __  | |  | | | |  \___ \    |  < | | '_ \ / _` |
 | |\ V /\__ \ |   | |  | | |__| | | |  ____) |   | . \| | | | | (_| |
 |_| \_/ |___/_|   |_|  |_|\____/  |_| |_____/    |_|\_\_|_| |_|\__, |
                                                                 __/ |
                                                                |___/ 
 _______                                                _   
|__   __|                                              | |  
   | | ___  _   _ _ __ _ __   __ _ _ __ ___   ___ _ __ | |_ 
   | |/ _ \| | | | '__| '_ \ / _` | '_ ` _ \ / _ \ '_ \| __|
   | | (_) | |_| | |  | | | | (_| | | | | | |  __/ | | | |_ 
   |_|\___/ \__,_|_|  |_| |_|\__,_|_| |_| |_|\___|_| |_|\__|
                                                            
                                                            
```
# Déroulement

Le tournoi se joue en deux phases:

* Une phase de groupe durant laquelle tous les joueurs d'un groupe se rencontrent à une occasion pour deux matches.
* Une phase de [playoffs à double élimination](https://en.wikipedia.org/wiki/Double-elimination_tournament) en [BO3](https://idioms.thefreedictionary.com/Best+of+Three) avec [double grande finale](https://help.toornament.com/structures/double-finals).

Les héros suivant sont exclus: Abathur, Azmodan, Ragnaros, The Lost Vikings, Zagara.
    
# Règlement

## Règles Générales

* La carte est `Cursed Hollow (Sandbox)`.
* Le héros est choisi dans la liste des 5 héros imposée pour le match.
* Chaque joueur choisi son héros sans connaître le choix de son adversaire. Tous les mécanismes sont autorisés pour arriver à cette fin : arbitre indépendant, annonce simultanée, ... 
* Interdiction de retourner à la zone de spawn, sauf en cas de décès (pas de hearthstone).
* Interdiction de quitter le couloir du milieu.
* Interdiction d'interagir avec les mercenaires.
* Interdiction de passer derrière le fort ennemi.
* Victoire en trois meurtres ou destruction des deux tours de défense.

## Phase de Groupe

* 1 point par match gagné lors d'une rencontre. Les possibilités sont donc `2-0`, `1-1` et `0-2`.
* En cas d'égalité au classement général, on tranchera à la différence meurtres-décès, puis par pile ou face si nécessaire.

## Phase de Playoffs

* La moitié supérieure du tableau de classement des groupes commence dans le winner bracket, la moitié inférieure dans le loser bracket (aka [skip first round](https://help.toornament.com/structures/introducing-the-skip-1st-round)).
* Au sein de chaque bracket, le placement sera effectué afin que les joueurs les mieux classés se rencontrent le plus tard possible (cf. [seeding](https://en.wikipedia.org/wiki/Seed_(sports))).

    
# Group Phase

| Group 1 | Group 2 | Group 3 |
| --- | --- | --- |
| Link | Railwayman | Zoiedberg |
| Luigi | Zangdar | Naleenae |
| Vash | Yoshi | Mario |
| Browser | Kör | Beurnzzz |
| Garandun | Zelda | Twisterwind |

## Group Phase Matches
 
Death it is